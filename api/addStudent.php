<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_id(optional)/fname/lname/address/dob/religion/gender(m/f)/photo_link
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. Add Child
 * 3. add room and map the id with school id
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            // add student
            // add student school mapping
            //if room_id is not null, add classroom student mapping
            $school = new School();
            $schoolId = $school->getSchoolIdFromUserId($userId);
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $fName = isset($params->fname)?trim($params->fname):null;
            if(is_null($fName) || $fName == ""){
                echo Message::FirstNameNull();  
            }else{
                $lName = isset($params->lname)?trim($params->lname):null;
                $address = isset($params->address)?trim($params->address):null;
                $dob =  isset($params->dob)?trim($params->dob):null;
                $religion = isset($params->religion)?trim($params->religion):null;
                $gender =  isset($params->gender)?trim($params->gender):null;
                $photoLink = isset($params->photo_link)?trim($params->photo_link):null;
                
                $studentId = $user->addStudent($userId,$schoolId,$roomId,$fName,$lName,$address,$dob,$religion,$gender,$photoLink);
                if(!is_null($roomId)&&$roomId!=""){
                    // add student room mapping
                    $user->addStudentRoomMapping($roomId,$studentId);
                }
                $data['status'] = 'SUCCESS';
                $data['data']['student_id']=$studentId;
                echo json_encode($data);
            }
        }else{
            echo Message::InvalidToken();
        }    
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}