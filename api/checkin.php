<?php

/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_id/student_id(comma seperated for multiple students)
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. add check-in for each student
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json"); 
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $studentId = isset($params->student_id)?trim($params->student_id):null;
            if(is_null($roomId)|| is_null($studentId)){
                echo Message::InvalidCheckInData() ;   
            }else{
                if($user->checkinStudents($roomId,$studentId)){
                    echo Message::Success();    
                }else{
                    echo Message::InvalidCheckInData() ;
                }                    
            }            
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}


?>