<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/email_id/fname/lname
 *  * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. check if email id already exists
 * 4. if not, add the user as staff with respective mappings
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $username = isset($params->email_id)?trim($params->email_id):null;
        $fName = isset($params->fname)?trim($params->fname):null;
        $lName = isset($params->lname)?trim($params->lname):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            if(Utils::validEmail($username)){
                if(!$user->checkUser($username)){
                    if(!(is_null($fName)||$fName=="")){
                        $school = new School();
                        $schoolId = $school->getSchoolIdFromUserId($userId);
                        $roleId = $user->getRoleId("STAFF");
                        $staffId = $user->addStaffToSchool($userId,$schoolId,$username,$fName,$lName,$roleId);
                        $data['status'] = 'SUCCESS';
                        $data['data']['staff_id']=$staffId;
                        echo json_encode($data);
                    }else{
                        echo Message::FirstNameNull();
                    }
                }else{
                    echo Message::UserExists();
                }                    
            }else{                
                echo Message::InvalidUsername();        
            }            
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>