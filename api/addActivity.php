<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_id/student_id(comma seperated if multiple students)/activity_id/staff_id/title/note/photo_link/timestamp)
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. Add Activity
 * 
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            //insert activity
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $activityId = isset($params->activity_id)?trim($params->activity_id):null;
            $staffId = isset($params->staff_id)?trim($params->staff_id):null;
            $studentId = isset($params->student_id)?trim($params->student_id):null;
            $title = isset($params->title)?trim($params->title):null;
            $note = isset($params->note)?trim($params->note):null;
            $timestamp = isset($params->timestamp)?trim($params->timestamp):null;
            $photoLink = isset($params->photo_link)?trim($params->photo_link):null;
            // insert activity
            if($user->addActivity($roomId,$activityId,$staffId,$studentId,$title,trim($note),$timestamp,$photoLink)){
                echo Message::Success();
            }else{
                echo Message::Error("Some error occured. Please try again.");
            }            
        }else{
            echo Message::InvalidToken();
        }
    }else{
        //return version error
        echo Message::VersionError();
     }   
}catch (Exception $ex) {
    echo Message::Error($ex);
}
?>