<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_name
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. Check if same name room exists
 * 4. If not, add the room else return error
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            //$sid = isset($_REQUEST['school_id'])?trim($_REQUEST['school_id']):null;
            $roomName = isset($params->room_name)?trim($params->room_name):null;
            $school = new School();
            $sid = $school->getSchoolIdFromUserId($userId);
            // check if room exists
            // if yes, return room exists error
            // if no, add room and return room id
            if($school->checkRoomName($roomName,$sid)){
                $roomId = $school->addClassRoom($roomName,$sid,$userId);
                $data['status'] = 'SUCCESS';
                $data['data']['room_id'] = $roomId;
                echo json_encode($data);
            }else{
                echo Message::ClassRoomNameExists();
            }
            
        }else{
            echo Message::InvalidToken();
        }
    }else{
        //return version error
        echo Message::VersionError();
     }   
}catch (Exception $ex) {
    echo Message::Error($ex);
}
?>