<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/student_id/room_id(optional)
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. Add Activity
 * 
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $studentId = isset($params->student_id)?trim($params->student_id):null;
            $activity = $user->getActivity($studentId,$roomId);
            if($activity){
                $data['status'] = 'SUCCESS';
                $data['data']['activity']=$activity;
                echo json_encode($data); 
            }else{
                echo Message::NoActivityForStudent();
            }
        }else{
            echo Message::InvalidToken();
        }
    }else{
        //return version error
        echo Message::VersionError();
     }   
}catch (Exception $ex) {
    echo Message::Error($ex);
}
?>