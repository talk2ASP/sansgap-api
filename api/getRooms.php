<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. get all the rooms for the given user id
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json"); 
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            // get all the rooms in alphabetical order with 1st room as default and there students list
            //$role = isset($_REQUEST['role'])?trim($_REQUEST['role']):'ADMIN';
            $school = new School();
            $sid = $school->getSchoolIdFromUserId($userId);
            $role = $user->getUserRole();
            switch(($role)){
                case 'ADMIN':
                    //get all rooms in the school
                    $rooms = $school->getAllRooms($sid);
                    if($rooms){
                        // get students for first(default) room.
                        // fetch classroom_id
                        $data['status'] = 'SUCCESS';
                        $data['data']['rooms']=$rooms;
                        $roomId = $rooms[0]['id'];
                        $students = $school->getStudentsOfRoom($roomId,$sid);
                        if($students){
                            $data['data']['students']=$students;
                        }else{
                            $data['data']['students']=null;
                        }
                        echo json_encode($data);
                    }else{
                        echo Message::NoRooms();
                    }
                                        
                    break;
                case 'STAFF':
                    // get only assigned rooms to the staff
                    //get Staff Id from User id
                    // get all rooms for this staff
                    // get students of this first room
                    $staff = $user->getStaffData();
                    if($staff){
                        $staffId = $staff['id'];
                        $rooms = $school->getRoomsOfStaff($staffId);
                        if($rooms){
                            $data['status'] = 'SUCCESS';
                            $data['data']['rooms']=$rooms;
                            $roomId = $rooms[0]['id'];
                            $students = $school->getStudentsOfRoom($roomId,$sid);
                            if($students){
                                $data['data']['students']=$students;
                            }else{
                                $data['data']['students']=null;
                            }
                            echo json_encode($data);
                        }else{
                            echo Message::NoRoomsForStaff();
                        }
                    }else{
                        //staff does not exist
                    }
                    break;
                case 'PARENT':
                    echo Message::NoRoomsForStaff();
                    break;
                default :
                    echo Message::NoRoomsForStaff();
                    break;
            }
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>