<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_id/month(00)/year(0000)
 *  * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. Get events for the room
 *
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';
try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $month = isset($params->month)?trim($params->month):null;
            $year = isset($params->year)?trim($params->year):null;
            if(is_null($roomId)||$roomId==""){
                echo Message::Error("Please select room to get event");
            }else{
                $school = new School();
                $events = $school->getEvent($roomId,$month,$year);
                if($events){
                    $data['status'] = 'SUCCESS';                    
                    $data['data']['events'] = $events;
                    echo json_encode($data);
                }else{
                    echo Message::Success("No event found");
                }
            }
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}

?>