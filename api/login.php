<?php

/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : username/password/v
 * 
 * Use Case:
 * 1. All users will send Username/password/version(v)
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';
try{    
    
    // check user
    // based on role get staff or parent details
    // for admin/teacher get school/staff/rooms/children
    // for parent get children
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){          
        $username = isset($params->username)?trim($params->username):null;
        $pwd = isset($params->password)?trim($params->password):null; 
        $user = new User();
        $school = new School();     
        if(Utils::validEmail($username)){
            $userData = $user->validateUser($username,Utils::getPassword($pwd));
            if($userData){
                if($userData['status']=="ACTIVE"){
                    //generate token
                    $token = Utils::generateToken();
                    //add token
                    $user->addToken($token);                                       
                    $role = $user->getRoleName($userData['role_id']);
                    if(strtoupper($role)=="ADMIN"){
                        $schoolId = $school->getSchoolIdFromUserId($userData['id']);
                        $schoolName = $school->getSchoolName($schoolId);
                        $stats = $school->getSchoolStats($schoolId);  
                        $data['status'] = 'SUCCESS';
                        $data['data'] = array("user_id"=>$userData['id'],"token"=>$token,"role"=>$role,"user_status"=>$userData['status'],"stats"=>$stats,"school_name"=>$schoolName);  
                    }else{
                        $data['status'] = 'SUCCESS';
                        $data['data'] = array("user_id"=>$userData['id'],"token"=>$token,"role"=>$role,"user_status"=>$userData['status']);   
                    }                    
                    
                    echo json_encode($data);                    
                }else{
                    echo Message::InactiveUsername();
                }                
            }else{
                //invalid username/password combination
                echo Message::InvalidLoginCredentials();
            }             
        }else{
            echo Message::InvalidUsername();
        }
        
    }else{
        //return version error
        echo Message::VersionError();
     }
 }catch(Exception $e){
    echo Message::Error($e);
 }


?>