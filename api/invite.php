<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/role/email_id/first_name/last_name/mobile/student_id
 *  * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. check if email id already exists
 * 4. if not, as per the role(staff/parent) add the user with respective mappings
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';
try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        
        $role = isset($params->role)?trim($params->role):null;
       
        $username = isset($params->email_id)?trim($params->email_id):null;
        
        //$schoolId = isset($_REQUEST['school_id'])?trim($_REQUEST['school_id']):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            //check user if it already exists
            //if not, check email id is valid
            //add the user with status invited with respective mapping            
            if(Utils::validEmail($username)){
                if(!$user->checkUser($username)){
                    //insert user
                    if(!is_null($role)){
                        $roleId = $user->getRoleId($role);
                        $school = new School();
                        $schoolId = $school->getSchoolIdFromUserId($userId);
                        $firstName = isset($params->first_name)?trim($params->first_name):null;
                        $lastName = isset($params->last_name)?trim($params->last_name):null;
                        $mobile = isset($params->mobile)?trim($params->mobile):null;
                        $studentId = isset($params->student_id)?trim($params->student_id):null;
                        if($user->addInviteUser($username,$roleId,$schoolId,$role,$firstName,$lastName,$mobile,$studentId)){
                            // send email                            
                            
                            echo Message::UserInvited();
                        }
                    }else{
                        echo Message::InvalidRole();
                    }                    
                }else{
                    echo Message::UserExists();
                }                    
            }else{                
                echo Message::InvalidUsername();        
            }            
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>