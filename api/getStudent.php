<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/room_id(optional)
 * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. if room_id is not null get students for that room
 * 3. else get students for room_id only
 * 3. add room and map the id with school id
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            //$schoolId = isset($_REQUEST['school_id'])?trim($_REQUEST['school_id']):null;
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            $school = new School();
            $schoolId = $school->getSchoolIdFromUserId($userId);
            if(is_null($roomId) || $roomId == ""){                
                // get all students of the school
                $students = $school->getAllStudentsOfSchool($schoolId);                                
            }else{
                //get students of the given room
                $students = $school->getStudentsOfRoom($roomId,$schoolId);
            }
            if($students){
                // get Parents 
                $ctr=0;
                foreach($students as $student){
                    $parent = $school->getParent($student['student_id']);                     
                    $students[$ctr++]['parent'] = $parent;                    
                }                                
                $data['status'] = 'SUCCESS';
                $data['data']['students'] = $students;
                echo json_encode($data);    
                
            }else{
                // no students found
                echo Message::NoStudentsFound();
            }
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>