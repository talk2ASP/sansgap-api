<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/staff_id/event_name/room_id/start/end/details
 *  * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. check if email id already exists
 * 4. if not, as per the role(staff/parent) add the user with respective mappings
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';
try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $staffId = isset($params->staff_id)?trim($params->staff_id):null;
            $eventName = isset($params->event_name)?trim($params->event_name):null;
            $start = isset($params->start)?trim($params->start):null;
            $end = isset($params->end)?trim($params->end):null;
            $details = isset($params->details)?trim($params->details):null;
            $roomId = isset($params->room_id)?trim($params->room_id):null;
            if(is_null($eventName)||$eventName==""||is_null($start)|| $start==""){
                echo Message::EventDetailsMissing();
            }else{
                $school = new School();
                $eventId = $school->addEvent($staffId,$roomId,$eventName,$start,$end,$details);
                if($eventId){
                    $data['status'] = 'SUCCESS';
                    $data['msg'] = "Event created sucesfully";
                    $data['data']['event_id'] = $eventId;
                    echo json_encode($data);
                }else{
                    echo Message::Error("Some unexpected error occured. Please try again.");
                }
            }
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>