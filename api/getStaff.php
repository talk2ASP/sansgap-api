<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v
 *  * 
 * Use Case:
 * Get all the Staff for a school
 * 
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. Get all the staff data for a given school
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $school = new School();
            $schoolId = $school->getSchoolIdFromUserId($userId);
            $staff = $school->getStaffForSchool();
            if($staff){
                $data['status'] = 'SUCCESS';
                $data['data']['staff']=$staff;
                echo json_encode($data);
            }else{
                echo Message::NoStaff();   
            }          
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>