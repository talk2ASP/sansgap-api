<?php

/**
 * @author Abhijit
 * @copyright 2016
 * 
 * POST API
 * 
 * Parameters : username/password/school name/pincode/v
 * 
 * Version: 1
 * 
 * Use Cases :
 * 
 * 1. Admin : New user send username/password/school_name/pincode/version
 * 2. Parent/Teacher : Invited User . Send Username/password/usertype/version
 * 
 */
 header("Access-Control-Allow-Origin: *");
 header("Content-type: application/json");
 require_once dirname(__FILE__) . '/../class/Utils.php';
 require_once dirname(__FILE__) . '/../class/User.php';
 require_once dirname(__FILE__) . '/../class/School.php';
 require_once dirname(__FILE__) . '/../class/Message.php';
 try{
     $params = json_decode(file_get_contents('php://input'));     
     $v = isset($params->v)?trim($params->v):null;     
     if(Utils::checkVersion($v)){        
        $username = isset($params->username)?trim($params->username):null;
        $pwd = isset($params->password)?trim($params->password):null;        
        $user = new User();
        $school = new School();
        // check user exists
        if(is_null($username)||is_null($pwd)){
            echo Message::InvalidParameters();   
        }else{
            $userData = $user->checkUser($username);
            if(!$userData){
                //get role
                $roleId = $user->getRoleId('ADMIN');
                if($roleId){
                    $school_name = isset($params->school_name)?strtoupper(trim($params->school_name)):null;
                    $pincode = isset($params->pincode)?trim($params->pincode):null;
                    if(is_null($school_name)||is_null($pincode)|| $school_name==""||$pincode==""){
                        echo Message::InvalidParameters();    
                    }else{
                       $schoolId = $school->addSchool($school_name,$pincode);                                 
                       $user->addUser($username,Utils::getPassword($pwd),$roleId,'ADMIN',$schoolId,"ACTIVE");
                       $token = Utils::generateToken();
                        //add token
                       $user->addToken($token);
                       // add Stats
                       //room count,staff count,children count,parent count
                       $stats = $school->getSchoolStats($schoolId);
                       $data['status'] = 'SUCCESS';
                       $data['data'] = array("user_id"=>$user->getUserId(),"token"=>$token,"role"=>'ADMIN',"user_status"=>"ACTIVE","stats"=>$stats,"school_name"=>$school_name);
                       echo json_encode($data); 
                    }
                    // add school                                                                    
                }else{
                    // role error
                    echo Message::InvalidRole();
                }                    
            }elseif(strtoupper($userData['status'])=="INVITED"){
                // update pwd
                $token = Utils::generateToken();
                $user->updateUser($userData['id'],Utils::getPassword($pwd),$token);
                $roleName = $user->getRoleName($userData['role_id']);
                //add token
                //$user->addToken($token);                
                $data['status'] = 'SUCCESS';
                $data['data'] = array("user_id"=>$userData['id'],"token"=>$token,"role"=>$roleName,"user_status"=>'ACTIVE');
                echo json_encode($data);
            }else{
                //user already exists
                echo Message::UserExists();
            }
        }
     }else{
        //return version error
        echo Message::VersionError();
     }
 }catch(Exception $e){
    echo Message::Error($e);
 }