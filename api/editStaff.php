<?php
/**
 * @author Abhijit
 * @copyright 2016
 * 
 * Parameters : user_id/token/v/staff_id/room_id(comma seperated if more than 1 school)/fname/lname/mobile1/mobile2/email/gender/designation/dob/photo_link/address/city/country
 *  * 
 * Use Case:
 * 1. token/user_id/v should match
 * 2. token and version should be valid
 * 3. check if email id already exists
 * 4. if not, add the user as staff with respective mappings
 */
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require_once dirname(__FILE__) . '/../class/Utils.php';
require_once dirname(__FILE__) . '/../class/User.php';
require_once dirname(__FILE__) . '/../class/Message.php';
require_once dirname(__FILE__) . '/../class/School.php';

try{
    $params = json_decode(file_get_contents('php://input')); 
    $v = isset($params->v)?trim($params->v):null;
    if(Utils::checkVersion($v)){        
        $token = isset($params->token)?trim($params->token):null;
        $userId = isset($params->user_id)?trim($params->user_id):null;
        $user = new User();
        if($user->validateToken($token, $userId)){
            $user->setUserId($userId);
            $staffId = isset($params->staff_id)?trim($params->staff_id):null;
            if(is_null($staffId)|| $staffId==""){
                echo Message::Error("Staff ID Missing");                    
            }else{
                $fName = isset($params->fname)?trim($params->fname):null;
                $lName = isset($params->lname)?trim($params->lname):null;
                $mobile1 = isset($params->mobile1)?trim($params->mobile1):null;
                $mobile2 = isset($params->mobile2)?trim($params->mobile2):null;
                $email = isset($params->email)?trim($params->email):null;
                $gender = isset($params->gender)?trim($params->gender):null;
                $designation = isset($params->designation)?trim($params->designation):null;
                $dob = isset($params->dob)?trim($params->dob):null;
                $photoLink = isset($params->photo_link)?trim($params->photo_link):null;
                $address = isset($params->address)?trim($params->address):null;
                $city = isset($params->city)?trim($params->city):null;
                $country = isset($params->country)?trim($params->country):"INDIA";                
                
                $user->updateStaffDetails($staffId,$fName,$lName,$mobile1,$mobile2,$email,$gender,$designation,$dob,$photoLink,$address,$city,$country);
                if(isset($params->room_id)){
                    $roomId = isset($params->room_id)?trim($params->room_id):null; 
                    $user->updateStaffRoomMapping($staffId,$roomId);   
                }                
                echo Message::Success();
            }            
        }else{
            echo Message::InvalidToken();
        }     
    }else{
        //return version error
        echo Message::VersionError();
     }
} catch (Exception $ex) {
    echo Message::Error($ex);
}
?>