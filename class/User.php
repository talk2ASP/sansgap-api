<?php
/**
 * Description of User
 *
 * @author Abhijit
 */
require_once dirname(__FILE__) . '/../DB/SGPDO.php';

class User{
    private $pdo;
    private $userId;
    private $staffId;
    private $staffObj;
    private $parentId;
    
    public function __construct() {
        $this->pdo = SGPDO::getInstance('local');        
    }
    
    public function setUserId($id){
        $this->userId = $id;
    }


    public function checkUser($email){
        $query = "select * from user where username = :username limit 1";
        $params = array(':username'=>$email);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) == 0){
            return FALSE;
        }
        else {
            return $result;            
        }
    }
    
    public function updateUser($id,$pwd,$token){
        $this->userId = $id;
        $query = "update user set password = :pwd,token = :token, status = 'ACTIVE' where id=:id limit 1";
        $params = array(":pwd"=>$pwd,":id"=>$id,":token"=>$token);
        $this->pdo->prepare($query)->doUpdate($params);
        return true;
    }
    
    public function addUser($email,$pwd,$roleId,$uType,$schoolId,$status){
        $query = "insert into user(username,password,role_id,status) values (:email,:pwd,:roleId,:status)";
        $params = array(":email"=>$email,":pwd"=>$pwd,":roleId"=>$roleId,":status"=>$status);
        $this->pdo->prepare($query)->doInsert($params);
        $this->userId = $this->pdo->lastInsertId();
        
        // add staff/parent
        switch($uType){
            case 'PARENT':
                addParent();
                break;
            case 'TEACHER':
                $this->addStaff($schoolId);
                break;
            case 'ADMIN':
                $this->addStaff($schoolId);
                break;
            default:
        }                
        return $this->userId;
    }
    
    private function addParent(){
        $query = "insert into parent(user_id) values (:uid)";   
        $params = array(":uid"=>$this->userId);
        $this->pdo->prepare($query)->doInsert($params);
        $this->parentId = $this->pdo->lastInsertId();
        return true;        
    }
    
    private function addStaff($schoolId){
        $query = "insert into staff(user_id) values (:uid)";   
        $params = array(":uid"=>$this->userId);
        $this->pdo->prepare($query)->doInsert($params);
        $this->staffId = $this->pdo->lastInsertId();
        
        //add school staff mapping
        $query = "insert into school_staff_mapping(staff_id,school_branch_id) values (:sid,:scid)";   
        $params = array(":sid"=>$this->staffId,":scid"=>$schoolId);
        $this->pdo->prepare($query)->doInsert($params);
        return true; 
    }
    
    public function getRoleId($role){
        $query = "select * from role where name = :role limit 1";
        $params = array(':role'=>strtoupper($role));
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
            return $result['id'];
        }
        else {
            return false;            
        }        
    }
    
    public function getRoleName($id){
        $query = "select * from role where id = :id limit 1";
        $params = array(':id'=>($id));
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
            return $result['name'];
        }
        else {
            return false;            
        }        
    }
    
    public function validateUser($username,$pwd){
        $query = "select * from user where username = :username and password = :pwd limit 1";
        $params = array(':username'=>$username,":pwd"=>$pwd);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
            $this->userId = $result['id'];            
            return $result;
        }
        else {
            return false;            
        }
    }
    
    public function getUserId(){
        return $this->userId;
    }
    
    public function getStaffData(){
        // get staff
        $query = "select * from staff where user_id = :id limit 1";
        $params = array(':id'=>$this->userId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
            $this->staffObj = $result;
            return $this->staffObj;            
        }
        else {
            return FALSE;            
        }
    }
    
    public function getAllStaffData($sid){
        // get staff
        $query = "select * from staff s , school_staff_mapping ssm, user u where school_branch_id = :sid and u.id=s.user_id and s.id = ssm.staff_id";
        $params = array(':sid'=>$sid);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) != 0){
           return $result;            
        }
        else {
            return false;            
        }
    }
    
    public function getAllStudentData($sid){
        $query = "select * from student s, school_student_mapping ssm where ssm.school_branch_id = :sid and s.id=ssm.student_id";
        $params = array(':sid'=>$sid);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) != 0){
           return $result;            
        }
        else {
            return false;            
        }        
    }
    
    public function addToken($token){
        $query = "update user set token = :token where id = :id limit 1";
        $params = array(":token"=>$token,":id"=>$this->userId);
        $this->pdo->prepare($query)->doUpdate($params);
        return true;
    }
    
    public function validateToken($token,$id){
        $query = "select * from user where id = :id and token = :token limit 1";
        $params = array(":id"=>$id,":token"=>$token);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
           return TRUE;            
        }
        else {
            return FALSE;            
        }
    }
    
    public function addStudent($userId,$schoolId,$roomId,$fName,$lName,$address,$dob,$religion,$gender,$photoLink){
        $query = "insert into student(first_name,last_name,address,dob,religion,gender,photo_link) values (:fname,:lname,:address,:dob,:religion,:gender,:plink)";
        $params = array(":fname"=>$fName,":lname"=>$lName,":address"=>$address,":dob"=>$dob,":religion"=>$religion,":gender"=>$gender,":plink"=>$photoLink);
        $this->pdo->prepare($query)->doInsert($params);
        $studentId = $this->pdo->lastInsertId();
        
        $query = "insert into school_student_mapping (school_branch_id,student_id,joining_date,user_id) values (:sbid,:sid,:time,:userid)";
        $params = array(":sbid"=>$schoolId,":sid"=>$studentId,":time"=>time(),":userid"=>$userId);
        $this->pdo->prepare($query)->doInsert($params);
        return $studentId;        
    }
    
    public function addStudentRoomMapping($roomId,$studentId){
        $query = "insert into classroom_student_mapping(classroom_id,student_id,joining_date) values (:cid,:sid,:time)";
        $params = array(":cid"=>$roomId,":sid"=>$studentId,":time"=>time());
        $this->pdo->prepare($query)->doInsert($params);
        return TRUE;
    }
    
    public function addStaffToSchool($userId,$schoolId,$username,$fName,$lName,$roleId){
        $query = "insert into user(username,role_id,status) values (:username,:rid,:status)";
        $params = array(":username"=>$username,":rid"=>$roleId,":status"=>"ACTIVE");
        $this->pdo->prepare($query)->doInsert($params);
        $userId = $this->pdo->lastInsertId(); 
        
        $query = "insert into staff(user_id,first_name,last_name) values (:uid,:fname,:lname)";
        $params = array(":uid"=>$userId,":fname"=>$fName,":lname"=>$lName);
        $this->pdo->prepare($query)->doInsert($params);
        $staffId = $this->pdo->lastInsertId();
        
        $query = "insert into school_staff_mapping(school_branch_id,staff_id) values (:scid,:sid)";
        $params = array(":scid"=>$schoolId,":sid"=>$staffId);
        $this->pdo->prepare($query)->doInsert($params);
        
        return $staffId;   
    }
    
    public function addInviteUser($email,$roleId,$schoolId,$role,$firstName,$lastName,$mobile,$studentId){
        $query = "insert into user(username,role_id,status) values (:email,:rid,:status)";
        $params = array(":email"=>$email,":rid"=>$roleId,":status"=>"INVITED");
        $this->pdo->prepare($query)->doInsert($params);
        $userId = $this->pdo->lastInsertId();
        switch(strtoupper($role)){
            case 'STAFF':
                //add staff user mapping
                // add school staff mapping
                
                $query = "insert into staff(user_id,first_name,last_name,mobile1) values (:uid,:fname,:lname,:mobile)";
                $params = array(":uid"=>$userId,":fname"=>$firstName,":lname"=>$lastName,":mobile"=>$mobile);
                $this->pdo->prepare($query)->doInsert($params);
                $staffId = $this->pdo->lastInsertId();
                
                $query = "insert into school_staff_mapping(school_branch_id,staff_id) values (:scid,:sid)";
                $params = array(":scid"=>$schoolId,":sid"=>$staffId);
                $this->pdo->prepare($query)->doInsert($params);
                break;
            case 'PARENT':
                //add parent user mapping
                $query = "insert into parent(user_id,first_name,last_name,mobile1) values (:uid,:fname,:lname,:mobile)";
                $params = array(":uid"=>$userId,":fname"=>$firstName,":lname"=>$lastName,":mobile"=>$mobile);
                $this->pdo->prepare($query)->doInsert($params);
                $parentId = $this->pdo->lastInsertId();
                if($studentId!=""||!is_null($studentId)){
                    $query = "insert into parent_student_mapping(parent_id,student_id) values (:pid,:sid)";
                    $params = array(":pid"=>$parentId,":sid"=>$studentId);
                    $this->pdo->prepare($query)->doInsert($params);
                }
                break;
            default:
                break;
        }
        return TRUE;
    }
    
    public function getUserRole(){
        $query = "select r.name as role from user u , role r where u.id=:uid and r.id=u.role_id limit 1";
        $params = array(":uid"=>$this->userId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
           return strtoupper($result['role']);            
        }
        else {
            return FALSE;            
        }
    }
    
    public function checkinStudents($roomId,$studentId){
        $studentArray = explode(',',$studentId);
        if(sizeof($studentArray)>0){
            $time = time();
            $query = "insert into checkin_students (room_id,user_id,student_id,time) values ";
            foreach($studentArray as $stuId){
                $query.= "($roomId,$this->userId,$stuId,$time),";
            }
            $query = substr($query,0,-1);
            $this->pdo->prepare($query)->doInsert();
            return TRUE;
        }else{
            return false;
        }
    }
    
    public function updateStaffDetails($staffId,$fName,$lName,$mobile1,$mobile2,$email,$gender,$designation,$dob,$photoLink,$address,$city,$country){
        $query = "update staff set first_name =:fname,last_name=:lname,mobile1=:mob1,mobile2=:mob2,email=:email,gender=:gender,dob=:dob,address=:address,city=:city,country=:country,photo_link=:plink,designation=:designation where id=:sid";
        $params = array(":fname"=>$fName,":lname"=>$lName,":mob1"=>$mobile1,":mob2"=>$mobile2,":email"=>$email,":gender"=>$gender,":dob"=>$dob,":address"=>$address,":city"=>$city,":country"=>$country,":plink"=>$photoLink,":designation"=>$designation,":sid"=>$staffId);
        $this->pdo->prepare($query)->doUpdate($params);
        return TRUE;
    }
    
    public function updateStaffRoomMapping($staffId,$roomId){
        $roomArray = explode(',',$roomId);
        $query = "delete from classroom_staff_mapping where staff_id = :sid";
        $params = array(":sid"=>$staffId);
        $this->pdo->prepare($query)->doDelete($params);
        $query = "insert into classroom_staff_mapping(staff_id,classroom_id) values " ;
        if(sizeof($roomArray)>0 && $roomId !=""){
            foreach($roomArray as $roomId){
                $query.= "($staffId,$roomId),";
            }
            $query = substr($query,0,-1);            
            $this->pdo->prepare($query)->doInsert();            
        }
        return TRUE;
    }
    
    public function addActivity($roomId,$activityId,$staffId,$studentId,$title,$note,$timestamp,$photoLink){
        $studentArray = explode(',',$studentId);
        $query = "insert into student_activity_mapping(student_id,activity_id,title,note,timestamp,room_id,staff_id,photo_link) values ";
        foreach($studentArray as $stuId){
            $query .= "($stuId,$activityId,'$title','$note',$timestamp,$roomId,$staffId,'$photoLink'),";
        }
        $query = substr($query,0,-1);                  
        $this->pdo->prepare($query)->doInsert();
        return TRUE;
    }
    
    public function getActivity($studentId,$roomId=null){
        if(is_null($roomId)){
            $query = "select * from activity a, student_activity_mapping sam where sam.student_id=:sid and a.id=sam.activity_id order by sam.timestamp desc";
            $params = array(":sid"=>$studentId);    
        }else{
            $query = "select * from activity a, student_activity_mapping sam where sam.student_id=:sid and a.id=sam.activity_id and sam.room_id=:rid order by sam.timestamp desc";
            $params = array(":sid"=>$studentId,":rid"=>$roomId);
        }
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
           return $result;            
        }
        else {
            return FALSE;            
        }        
    }
}
?>