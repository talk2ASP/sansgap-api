<?php
class Message{
    public static function Success($msg=""){
        $data['status'] = 'SUCCESS';
        $data['msg'] = $msg;
        return json_encode($data);
    }
    public static function Error($e){
        $data['status'] = 'ERROR';
        $data['msg'] = "Unexpected error occured. Please try again." . " Error Details: " . $e;
        return json_encode($data);
    }
    
    public static function InvalidParameters(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Some parameters are missing.";
        return json_encode($data);    
    } 
    
    public static function VersionError(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Version Number not specified or we have stopped supporting the version you are using.";
        return json_encode($data);
    }

    public static function UserExists(){
        $data['status'] = 'ERROR';
        $data['msg'] = "User with this email id already exists";
        return json_encode($data);
    }
    
    public static function InvalidRole(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Role provided does not exist";
        return json_encode($data);
    } 
    
    public static function InvalidUsername(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Invalid username. Please use correct email id";
        return json_encode($data);    
    }
    
    public static function InactiveUsername(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Inctive user. Please contact admin/owner";
        return json_encode($data);  
    }
    
    public static function InvalidLoginCredentials(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Invalid username or password. Please try again";
        return json_encode($data);    
    }
    
    public static function InvalidToken(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Invalid token. Please try again";
        return json_encode($data);    
    }
    
    public static function ClassRoomNameExists(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Classroom with the given name already exists. Please provide a new name.";
        return json_encode($data);    
    }
    
    public static function NoRooms(){
        $data['status'] = 'ERROR';
        $data['msg'] = "There are no rooms. Please create new room.";
        return json_encode($data);
    }
    
    public static function NoRoomsForStaff(){
        $data['status'] = 'ERROR';
        $data['msg'] = "There are no rooms assigned to the staff. Please create assign room.";
        return json_encode($data);
    }
    
    public static function FirstNameNull(){
        $data['status'] = 'ERROR';
        $data['msg'] = "First name cannot be blank.";
        return json_encode($data);
    }

    public static function NoStudentsFound(){
        $data['status'] = 'ERROR';
        $data['msg'] = "There are no students. Please add new students.";
        return json_encode($data);    
    }
    
    public static function UserInvited(){
        $data['status'] = 'SUCCESS';
        $data['msg'] = "Invitation sent succefully.";
        return json_encode($data);
    }
    
    public static function InvalidCheckInData(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Check-in data missing or invalid. Please try again.";
        return json_encode($data);   
    }
    
    public static function NoStaff(){
        $data['status'] = 'ERROR';
        $data['msg'] = "No staff found. Please add new Staff.";
        return json_encode($data);   
    }
    
    public static function NoActivityForStudent(){
        $data['status'] = 'ERROR';
        $data['msg'] = "No activity found for the student.";
        return json_encode($data);
    }
    
    public static function EventDetailsMissing(){
        $data['status'] = 'ERROR';
        $data['msg'] = "Some or all of the Event details are missing. Please try again.";
        return json_encode($data);   
    }
    
    public static function ParentDoesNotExist(){
        $data['status'] = 'ERROR';
        $data['msg'] = "No parent found.";
        return json_encode($data);    
    }
}
?>