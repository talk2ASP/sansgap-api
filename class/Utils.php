<?php
/**
 * Description of Utils
 *
 * @author Abhijit
 */
class Utils{
    
    private static $v=1;
    private static $TOKEN_VERSION = 1;
    
    public static function checkVersion($ver){
        return ($ver==self::$v)?true:false;
    }
    
    public static function validEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    public static function generateToken(){
        return base64_encode(self::$TOKEN_VERSION . rand(1,time()));
    }
    
    public static function decodeToken($token){
        return base64_decode($token);
    }
    
    public static function getPassword($pwd){
        return md5($pwd);
    }
}
?>