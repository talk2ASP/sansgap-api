<?php
/**
 * Description of School
 *
 * @author Abhijit
 */
require_once dirname(__FILE__) . '/../DB/SGPDO.php';

class School{
    private $pdo;
    private $schoolId;
    
    public function __construct() {
        $this->pdo = SGPDO::getInstance('local');        
    }
    
    public function addSchool($name,$pincode){
        //check area
        $areaId = $this->checkPincode($pincode);
        if(!$areaId){
            $areaId = $this->addArea($pincode);
        }
        //add school
        $query = "insert into school_branch(name,area_id) values (:name,:areaId)";
        $params = array(":name"=>$name,":areaId"=>$areaId);
        $result = $this->pdo->prepare($query)->getResult($params);    
        $this->schoolId = $this->pdo->lastInsertId();
        return $this->schoolId;
    }
    
    private function checkPincode($pincode){
        $query = "select * from area where pincode = :pincode limit 1";
        $params = array(":pincode"=>$pincode);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) == 0){
            return false;
        }
        else {
            return $result['id'];            
        }
    }
    
    private function addArea($pincode){
        $query = "insert into area(city_id,pincode) values (:cityId,:pincode)";
        $params = array(":cityId"=>1,":pincode"=>$pincode);
        $this->pdo->prepare($query)->doInsert($params);
        return $this->pdo->lastInsertId();
    }
    
    public function getSchoolData($staffId){
        //get school data
        $query = "select * from school_branch sb, school_staff_mapping ssm where ssm.staff_id = :sid and sb.id = ssm.school_branch_id limit 1";
        $params = array(":sid"=>$staffId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) != 0){
            $this->schoolId = $result['sb.id'];
            return $result;
        }
        else {
            return false;            
        }
    }
    
    public function getAllRooms($sid){
        $query = "select cr.id as id , cr.name as name from classroom cr, classroom_school_mapping csm where csm.school_branch_id = :sid and cr.id = csm.classroom_id order by cr.name";
        $params = array(":sid"=>$sid);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) != 0){
            return $result;
        }
        else {
            return FALSE;            
        }
    }
    
    public function getSchoolId(){
        return $this->schoolId;
    }
    
    public function checkRoomName($name,$schoolId){
        $query = "select * from classroom c, classroom_school_mapping csm where c.name = :name and csm.school_branch_id = :schoolId and c.id=csm.classroom_id";
        $params = array(":name"=>$name,":schoolId"=>$schoolId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) == 0){
            return TRUE;
        }
        else {
            return false;            
        }
    }
    
    public function addClassRoom($roomName,$schoolId,$userId){
        $query = "insert into classroom(name) values (:roomname)";
        $params = array(":roomname"=>$roomName);
        $this->pdo->prepare($query)->doInsert($params);
        $roomId =  $this->pdo->lastInsertId();
        
        $query = "insert into classroom_school_mapping(school_branch_id,classroom_id,user_id,creation_datetime) values (:schoolId,:roomId,:userId,:time)";
        $params = array(":schoolId"=>$schoolId,":roomId"=>$roomId,":userId"=>$userId,":time"=>time());
        $this->pdo->prepare($query)->doInsert($params);
        return $roomId;
    }
    
    public function getStudentsOfRoom($roomId,$schoolId){
        $query = "select * from student s, classroom_student_mapping csm , classroom_school_mapping cssm where csm.classroom_id=:rid and s.id = csm.student_id and cssm.classroom_id=:rid and cssm.school_branch_id =:sid";
        $params = array(":rid"=>$roomId,":sid"=>$schoolId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getAllStudentsOfSchool($schoolId){
        $query = "select * from student s , school_student_mapping ssm where s.id=ssm.student_id and ssm.school_branch_id=:sid";
        $params = array(":sid"=>$schoolId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getParent($studentId){
        $query = "select p.*,psm.parent_id,psm.student_id,u.status from  parent p , parent_student_mapping psm ,user u where p.user_id = u.id and p.id=psm.parent_id and psm.student_id = :sid";
        $params = array(":sid"=>$studentId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
        
    }
    
    public function getRoomsOfStaff($staffId){
        $query = "select cr.id as id,cr.name as name from classroom cr, classroom_staff_mapping csm where csm.staff_id = :sid and cr.id=csm.classroom_id";
        $params = array(":sid"=>$staffId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getSchoolIdFromUserId($userId){
        $query = "select ssm.school_branch_id as school_id from school_staff_mapping ssm, staff s where s.user_id=:uid and s.id=ssm.staff_id limit 1";
        $params = array(":uid"=>$userId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) !=0){
            $this->schoolId = $result['school_id'];
            return $this->schoolId;
        }else{
            return FALSE;
        }
    }
    
    public function getSchoolName($schoolId){
        $query = "select * from school_branch where id=:sid";
        $params = array(":sid"=>$schoolId);
        $result = $this->pdo->prepare($query)->getResult($params);
        return $result['name'];
    }
    
    public function getStaffForSchool(){
        $query = "select * from staff s, school_staff_mapping ssm where ssm.school_branch_id = :sid and ssm.staff_id = s.id sort by s.first_name";
        $params = array(":sid"=>$this->schoolId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function addEvent($staffId,$roomId,$eventName,$start,$end,$details){
        $time = time();
        $query = "insert into event (name,start,end,details,staff_id) values (:name,:start,:end,:details,:sid)";
        $params = array(":name"=>$eventName,":start"=>$start,":end"=>$end,":details"=>$details,":sid"=>$staffId);
        $this->pdo->prepare($query)->doInsert($params);
        $eventId =  $this->pdo->lastInsertId();
        
        $roomArray = explode(',',$roomId); 
        $query = "insert into event_room_mapping(room_id,event_id,timestamp) values ";
        foreach($roomArray as $roomId){
            $query.= "($roomId,$eventId,$time),";               
        }   
        $query = substr($query,0,-1);                  
        $this->pdo->prepare($query)->doInsert();
        return $eventId;
    }
    
    public function getEvent($roomId,$month,$year){
        $query = "select * from event e , event_room_mapping erm where e.id = erm.event_id and erm.room_id = :rid and ((FROM_UNIXTIME((start),'%m%Y')=:my) || (FROM_UNIXTIME((end),'%m%Y') = :my)) order by e.start asc ";
        $params = array(":rid"=>$roomId,":my"=>$month.$year);        
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getParentFromUserId($userId){
        $query = "select p.*,u.status from parent p, user u where p.user_id = u.id and u.id = :uid";
        $params = array(":uid"=>$userId);
        $result = $this->pdo->prepare($query)->getResult($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getChildrenForParent($parentId){
        $query = "select * from student s, parent_student_mapping psm where s.id=psm.student_id and psm.parent_id=:pid";
        $params = array(":pid"=>$parentId);
        $result = $this->pdo->prepare($query)->getResults($params);
        if(sizeof($result) !=0){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public function getSchoolStats($sid){
       //room count
       $query = "select count(*) as count from classroom_school_mapping csm where csm.school_branch_id=:sid";
       $params = array(":sid"=>$sid);
       $result = $this->pdo->prepare($query)->getResult($params);
       $rcount = $result['count'];
       
       // staff count
       $query = "select count(*) as count from school_staff_mapping ssm where ssm.school_branch_id=:sid";
       $params = array(":sid"=>$sid);
       $result = $this->pdo->prepare($query)->getResult($params);
       $scount = $result['count'];
       
       //student count
       $query = "select count(*) as count from school_student_mapping ssm where ssm.school_branch_id=:sid";
       $params = array(":sid"=>$sid);
       $result = $this->pdo->prepare($query)->getResult($params);
       $stcount = $result['count'];
       
       //parent count
       $query = "select count(*) as count from parent_student_mapping psm, school_student_mapping ssm where ssm.school_branch_id=:sid and ssm.student_id=psm.student_id";
       $params = array(":sid"=>$sid);
       $result = $this->pdo->prepare($query)->getResult($params);
       $pcount = $result['count'];
       
       $data=array();
       $data['room_count']=$rcount;
       $data['staff_count']=$scount;
       $data['student_count']=$stcount;
       $data['parent_count']=$pcount;
       return $data;
    }
}
?>